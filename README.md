# Gradient shadow in pure CSS

![alt text](https://res.cloudinary.com/tungu/image/upload/v1580845275/gradient-shadow-btn_sv1s4j.jpg)

_Usage_

_Copy the gradient-shadow.css file and paste it into your projects_

_and import it into your index.html_

###### HTML

```html
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Gradient Shadow</title>
    <link rel="stylesheet" href="./styles/gradient-shadow.css" />
  </head>
  <body>
    <button>Let's Go !</button>
  </body>
</html>
```

### OR

_Declare colors_

```css
:root {
  --purple: #7f00ff;
  --pink: #e100ff;
}
```

_Styling our button_

```css
button {
  font-family: "Roboto", sans-serif;
  font-size: 16px;
  position: relative;
  outline: none;
  border: none;
  -webkit-appearance: none;
  -webkit-tap-highlight-color: transparent;
  cursor: pointer;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
  padding: 0.75em 1.75em;
  border-radius: 50px;
  display: inline-block;
  color: #fff;

  /* Our gradient */
  background: -webkit-gradient(
    linear,
    left top,
    right top,
    from(var(--purple)),
    to(var(--pink))
  );
  background: linear-gradient(to right, var(--purple), var(--pink));
}
```

_Now, let's create our shadow_

```css
button::after {
  content: "";
  position: absolute;
  z-index: -1;
  bottom: -10px;
  left: 5%;
  height: 110%;
  width: 90%;
  opacity: 0.8;
  border-radius: 50px;

  /* Declaring our shadow color inherit from the parent (button) */
  background: inherit;

  /* Blurring the element for shadow effect */
  -webkit-filter: blur(6px);
  -moz-filter: blur(6px);
  -o-filter: blur(6px);
  -ms-filter: blur(6px);
  filter: blur(6px);

  /* Transition for the magic */
  -webkit-transition: all 0.2s;
  transition: all 0.2s;
}
```

_We have our button and her shadow, now change the style for hover_

```css
button:hover::after {
  /* Changing blur effect */
  -webkit-filter: blur(4px);
  -moz-filter: blur(4px);
  -o-filter: blur(4px);
  -ms-filter: blur(4px);
  filter: blur(4px);

  /* And change the style properties */
  width: 100%;
  bottom: -5px;
  left: 0;
}
```

_For more interactivity, change the active effect_

```css
button:hover:active::after {
  -webkit-filter: blur(10px);
  -moz-filter: blur(10px);
  -o-filter: blur(10px);
  -ms-filter: blur(10px);
  filter: blur(10px);
}
```

### Easy no ?

_For some bug and impovements on IE and Edge_

```css
/* Edge does not support blur effect, so we can just delete the ::after element and replace it by a box-shadow */

@supports (-ms-ime-align: auto) {
  button {
    -webkit-box-shadow: 0 20px 20px -15px rgba(127, 0, 255, 0.8);
    box-shadow: 0 20px 20px -15px rgba(127, 0, 255, 0.8);
  }
  button::after,
  button:hover::after,
  button:active::after {
    display: none;
  }
}

/* And IE does not support CSS variables and blur effect, so we had to make the same and recreate the background-gradient */

@media screen and (-ms-high-contrast: active), (-ms-high-contrast: none) {
  /* Fix css var by calling the hexa colors */
  button {
    background: -webkit-gradient(
      linear,
      left top,
      right top,
      from(#7f00ff),
      to(#e100ff)
    );
    background: linear-gradient(to right, #7f00ff, #e100ff);
    -webkit-box-shadow: 0 20px 20px -15px rgba(127, 0, 255, 0.8);
    box-shadow: 0 20px 20px -15px rgba(127, 0, 255, 0.8);
    border-collapse: separate;
  }
  /* Deleting the shadow */
  button::after,
  button:hover::after,
  button:active::after {
    display: none;
  }
}
```

[Final result (linear-gradient-shadow)](https://linear-gradient-shadow.surge.sh/)

[A live version is here](https://codepen.io/tunguska/pen/KovRbg)
